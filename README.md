## ASE_math: Anomalous Skin Effect

**Author:** Sergio Calatroni (sergio.calatroni _at_ cern.ch)

**Copyright:** CERN (2010 - 2020)

**License:** GNU General Public Licence version 3 (GPL Version 3)

## Introduction

This Mathematica notebook calculates the RF surface impedance of copper in the Anomalous Skin Effect (ASE) regime.
The ASE regime appears when the electron mean free path is larger than the standard skin depth, calculated from Maxwell's equations. This happens either for very low temperatures, or very large frequencies.
The calculation is performed following the classical paper [1], assuming copper material parameters from [2-5] and can be done for any temperature and magnetic field (thus including magnetoresistance). Details on how the calculation is performed, and on the specific formulae for the temperature-dependent electrical resistivity and magnetoresistance will be discussed in a future publication.

A few examples are included in the notebook:
-	Surface impedance at 1 GHz, in application to the LHC beam screen
-   Copper resistivity and surface resistance at 400 MHz as a finction of temperature from [6]
-	Surface resistance at 8 GHz, both in ASE and normal regimes, at 0 T and 8 T.
-	Calculations for [7]
-	Explicit plot of magnetoresistance effects for RRR=100 copper, for various B
-	Calculation of the plots of [8]
-	Calculation of the crossover temperature and characteristic length between ASE and normal regimes at 8.5 GHz for copper  

![](https://codimd.web.cern.ch/uploads/upload_ce30d36358f1ca1209ce5a187ad588c1.png)


## Code

The Mathematica notebook is available on [**GitLab**](https://gitlab.cern.ch/scala/anomalos-skin-effect/-/tree/master) 


## References

1. G.E.H. Reuter and E.H. Sondheimer, "The theory of the anomalous skin effect in metals", Proc. Roy. Soc. A195 (1948) 336-364
2. J.G. Hust and A.B. Lankford, "Thermal conductivity of aluminum, copper, iron, and tungsten for temperatures from 1 K to the melting point", NBSIR 84-3007, National Bureau of Standards (Boulder, Colorado, USA, 1984)
3. E. Drexler, N. Simon and R. Reed, "Properties of copper and copper alloys at cryogenic temperatures", NIST Monograph 177,Chapter 8, US Government Printing Office (Washington DC, USA, 1992)
4. J.Bass, "Size effects", in Landolt-Börnstein, "Electrical resistivity of pure metals and dilute alloys" K.-H. Hellwege, J.L. Olsen (eds.), Vol 15A "Electrical Resistivity, Kondo and Spin Fluctuation Systems, Spin Glasses and Thermopower", Springer-Verlag (Berlin Heidelberg, DE, 1983)
5. C. Kittel, "Introduction to solid state physics", 5th edition, Wiley (New York, USA, 1976)
6. S. Calatroni, “Materials and Properties: Thermal and Electrical Characteristics”, In: Proceedings of CAS on Vacuum Technology (2017) xx–yy.
7. S. Calatroni, M. Arzeo, S. Aull, M. Himmerlich, P. Costa Pinto, W. Vollenberg, B. Di Girolamo, P. Cruikshank, P. Chiggiato, D. Bajek, S. Wackerow and A. Abdolvand, "Cryogenic surface resistance of copper: Investigation of the impact of surface treatments for secondary electron yield reduction", Phys. Rev. acc. Beams 22, 063101 (2019)
8. E. Métral, "Beam screen issues", Proc. HE-LHC Workshop, pp.83-89 (2010)

